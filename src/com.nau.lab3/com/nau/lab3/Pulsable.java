package com.nau.lab3;

public interface Pulsable {
    public void pulse();
}
