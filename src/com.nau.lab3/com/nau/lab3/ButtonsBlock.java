package com.nau.lab3;

import javafx.event.ActionEvent;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;

public class ButtonsBlock {
    private final RadioButton start;
    private final RadioButton stop;

    public ButtonsBlock() {
        ToggleGroup toggleGroup = new ToggleGroup();
        start = new RadioButton("Start");
        stop = new RadioButton("Stop");
        start.setSelected(true);

        start.setToggleGroup(toggleGroup);
        stop.setToggleGroup(toggleGroup);
    }

    public RadioButton getStop() {
        return stop;
    }

    public void setStartEvent(Thread thread) {
        start.setOnAction((ActionEvent event) -> {
            thread.resume();
        });
    }

    public void setStopEvent(Thread thread) {
        stop.setOnAction((ActionEvent event) -> {
            thread.suspend();
        });
    }

    public RadioButton getStart() {
        return start;
    }
}
